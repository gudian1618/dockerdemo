import org.junit.Test;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.JedisShardInfo;
import redis.clients.jedis.ShardedJedis;
import redis.clients.jedis.ShardedJedisPool;

import java.util.ArrayList;
import java.util.List;

/**
 * @author gudian1618
 * @version v1.0
 * @date 2020/11/9 10:39 下午
 */

public class Test1 {

    @Test
    public void test2() {
        JedisPoolConfig cfg = new JedisPoolConfig();
        cfg.setMaxTotal(500);
        cfg.setMaxIdle(20);

        List<JedisShardInfo> shards = new ArrayList<JedisShardInfo>();
        shards.add(new JedisShardInfo("127.0.0.1", 7000));
        shards.add(new JedisShardInfo("127.0.0.1", 7001));
        shards.add(new JedisShardInfo("127.0.0.1", 7002));

        ShardedJedisPool pool = new ShardedJedisPool(cfg, shards);

        ShardedJedis j = pool.getResource();
        for (int i = 0; i < 100; i++) {
            j.set("key" + i, "value" + i);
        }

        pool.close();
    }

}
